package com.automation.report;

public interface ReporterConstants {
	String configReporterFile = "resources/gallopReporter.properties";

	/*
	 * Whether Being Run On Single Browser Or Multiple Browser In Multiple
	 * Threads
	 */
	/* browserName */
	String BROWSER_NAME = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "browserName");
	String TEST_WITH_OPEN_BROWSER = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "alwaysopenbrowser");
	
	String PLATFORM_TYPE = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "platform");
	String APPIUM_URL = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "appiumURL");
	String APP_PACKAGE = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "appPackage");
	String APP_ACTIVITY = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "appActivity");
	String DEVICE_ID = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "deviceID");
	String DEVICE_NAME = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "deviceName");
	String PLATFORM_NAME = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "platformName");
	String PLATFORM_VERSION = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "platformVersion");
	String Timeout = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "timeoutInSec");
	String bitBucket_usr = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "bitBucket_usr");
	String bitBucket_pswd = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "bitBucket_pswd");
	String bitBucket_source = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "bitBucket_source");
	String bitBucket_destination = ConfigFileReadWrite.read(ReporterConstants.configReporterFile,
			"bitBucket_destination");
	String packagename_libs = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "packageName_libs");
	String testRailUrl = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "testRailUrl");
	String testRail_userName = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "testRail_userName");
	String testRail_paswd = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "testRail_paswd");
	String db_ExecutionComments = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "db_ExecutionComments");
	String downloaded_file_path = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "downloaded_file_path");
	String rackspace_loc = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "rackspace_loc");
	String Jenkins_path = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "Jenkins_path");
	String TT_ENV_URL = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttenv");
	String TT_QA_ENV = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttqa");
	String TT_STAGING_ENV = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttstaging");
	String TT_MYTRIPS_QA_ENV =  ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttMyTripsQa");
	String TT_MYTRIPS_STAGING_ENV =  ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttMyTripsStaging");
	String TT_QA_PASSWORD = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttqaPassword");
	String TT_QA_UserName = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttqaUserName");
	String TT_STAGING_USERNAME = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttstagingUserName");
	String TT_STAGING_PASSWORD = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttstagingPassword");
	String TT_PROD_ENV = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttprod");
	String EVERBRIDGE = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "everbridge"); 
	String TT_MOB_QAENV = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttmobqa"); 
	String TT_MOB_STAGING = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttmobstaging"); 
	String TT_MOB_QA_PASSWORD = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttmobQaPassword");
	String TT_MOB_QA_UserName = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttmobQaUserName");
	String TT_MOB_STG_PASSWORD = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttmobStagePassword");
	String TT_MOB_STG_UserName = ConfigFileReadWrite.read(ReporterConstants.configReporterFile, "ttmobStageUserName");

}