package com.testRail.createArtifacts;

import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.testng.annotations.Factory;

import com.automation.report.ConfigFileReadWrite;

public class TestRailDriver {
	
	public final static Logger LOG = Logger.getLogger(TestRailDriver.class);
	public static CreateArtifactsTestRail createTR;

	/**
	 * This method will decide the test run will happen in jenkins 
	 * or local env.
	 * 
	 * This function executes the class objects built dynamically from Testrail
	 * 
	 * @return class instantiated  object
	 * @throws Throwable 
	 */
	@Factory
	public Object[] FactoryClass() throws Throwable
	{
		Object[] data = null;
		if(System.getenv("projectName")!=null){
			setUpTestPlanJenkins(); //Set Up Testing Artifacts in Test Rail
			System.out.println("Test Plans and Test Runs have been setup successfully.");
		}
		
		return data;

	}
	/**
	 * This function takes the input params like Project Name, Test Plan Name, Browser name from the jenkins and 
	 * set up the Test Plans accordingly in Test Rail
	 * @throws Throwable
	 */
	private void setUpTestPlanJenkins() throws Throwable
	{
		LOG.info("Setting Up Testing artifacts (Milestones, Test Plan and Test Runs) in Test Rail from Jenkins.");
		setArtifactsInfo();
		createTR=new CreateArtifactsTestRail();
		createTR.createFoldersInTestRail();
		createTR.writeTestRunsInfoToConfigFile();
	}
	/**
	 * This function takes the input params from Jenkins and sets them up in gallopReporter.properties file
	 * @throws Throwable
	 */
	private void setArtifactsInfo() throws Throwable
	{
		String projectName = System.getenv("projectName").trim().toString();
		LOG.info("projectName :" + projectName);
		ConfigFileReadWrite.writeToReporterFile("testRail_projectName", projectName);
		
		String milestoneName = System.getenv("milestoneName").trim().toString();
		LOG.info("milestoneName :" + milestoneName);
		ConfigFileReadWrite.writeToReporterFile("testRail_milestoneName", milestoneName);
		
		String subMsName = System.getenv("subMilestoneName").trim().toString();
		LOG.info("subMilestone :" + subMsName);
		ConfigFileReadWrite.writeToReporterFile("testRail_subMilestoneName", subMsName);
		
		String testPlanName = System.getenv("testPlanName").trim().toString();
		LOG.info("testPlanName :" + testPlanName);
		ConfigFileReadWrite.writeToReporterFile("testRail_testPlanName", testPlanName);
		
		String browserName = System.getenv("browserName").trim().toString();
		LOG.info("browserName :" + browserName);
		ConfigFileReadWrite.writeToReporterFile("testRail_browserName", browserName);
		
		LOG.info("Artifcats info has been setup successfully in Reporter.properties file..");
	}
	
}
